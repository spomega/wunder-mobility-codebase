#!/usr/bin/env bash

docker-compose down

docker-compose up -d --build

docker-compose exec app php artisan config:cache


>&2 echo "Bootstrapping application Please wait....."
sleep 10
>&2 echo "Application started :)"

docker-compose exec app php artisan migrate
>&2 echo "database migration done..."

docker-compose exec app vendor/bin/phpunit
>&2 echo "testing completed..."

>&2 echo "Application is running at http://localhost:8080"
sleep 3
exit 0
