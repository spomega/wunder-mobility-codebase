# Wunder Fleet User Registration

Web app to register wunder fleet users  

 -  [Demo link deployed on AWS](http://3.123.35.163/wunderfleet/)

# Requirements
- composer
- php v7.1.3
- mysql v5.7.22  
- Docker v18.06.0  
- Laravel Framework v5.8 


# Installation

#### Setup Details -without docker

- git clone this repository
 change directory into the project folder

- The cloned code comes with .env.example file in the root of the project.

  You must rename this file to just .env.
  
- Execute the command 'composer install' in the command line/terminal.

- Create your database on your server and on your .env file update the following lines:
 
  DB_CONNECTION=mysql  
  DB_HOST=127.0.0.1  
  DB_PORT=3306  
  DB_DATABASE=homestead  
  DB_USERNAME=homestead  
  DB_PASSWORD=secret  
 
  Change these lines to reflect your new database settings.
  
- Execute command 'php artisan key:generate' to generate your APP_KEY.

- Execute 'php artisan migrate' to setup the database.  
NB: if you encounter any errors check database connection settings .env.
- Execute 'vendor/bin/phpunit' to run tests.  
 
Finally execute php artisan serve to run the project as access web application in the browser at http://localhost:8000/  
NB: if you get any permission errors on .log files execute command 'chmod -R 777 storage/'.


### Setup Details  -with Docker  
- git clone this repository
 change directory into the project folder.
- Update  .env file to reflect your new database settings.
  
- Execute command  'sh ./docker_run.sh' to create and start php,mysql,nginx containers and automate setup  

    NB:shutdown mysql on your local machine or server to avoid conflict on port 3306 if you get any errors on port conflict  
    
 Run the project as access web application in the browser at http://localhost:8080/
 
 
 # Database Dump File  
 
- Database dump can be located database/dumps/wunder_database_dump.sql




# Possible Performance Optimization  

- Assets Minifying with laravel mix,once minified the assets become smaller in size hence be retrieved
 faster, which will speed up the performance the application.  



# Things That Could Have Been Done Better  
- Use DRY principle to optimize payment server access code in test class and controller.

- Frontend Testing with Laravel dusk
