<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Wunder Fleet User Registration</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link hre="css/jquery.steps.css" rel="stylesheet">

    </head>
    <body>
<div class="container">

    <div class="row">
        <form id="user-form" action="#">
            <h3>JOIN THE RIDE</h3>
            <fieldset>
                <legend>Personal Information</legend>

                <label for="first_name">First Name*</label>
                <input id="first_name" name="first_name" type="text" placeholder="Neo" class="required,form-control">
                <label for="last_name">Last Name*</label>
                <input id="last_name" name="last_name" type="text" placeholder="Trinity" class="required">
                <label for="telephone">Telephone*</label>
                <input id="telephone" name="telephone" type="tel" class="required" placeholder="+49 xxxx xxx">
                <p>(*) Required</p>
            </fieldset>

            <h3>WHERE ARE YOU?</h3>
            <fieldset>
                <legend>Address</legend>

                <label for="street_name">Street Name *</label>
                <input id="street_name" name="street_name" type="text" class="required,form-control">
                <label for="house_number">House Number *</label>
                <input id="house_number" name="house_number" type="text" class="required,form-control">
                <label for="zip_code">Zip Code*</label>
                <input id="zip_code" name="zip_code" type="text" class="required,form-control" placeholder="4444">
                <label for="city">City</label>
                <select name="city" id="city" class="required,form-control">
                    <option value="Amsterdam">Amsterdam</option>
                    <option value="Alicante">Alicante</option>
                    <option value="Barcelona">Barcelona</option>
                    <option value="Berlin">Berlin</option>
                    <option value="Brussels">Brusssels</option>
                    <option value="Cardiz">Cardiz</option>
                    <option value="Duseldorf">Duseldorf</option>
                    <option value="Hamburg">Hamburg</option>
                    <option value="London">London</option>
                </select>
                <p>(*) Required</p>
            </fieldset>

            <h3>PAYMENT INFORMATION</h3>
            <fieldset>
                <legend>Payment Information</legend>

                <label for="account_name">Account Name*</label>
                <input id="account_name" name="account_name" type="text" placeholder="Neo Trinity" class="required">
                <label for="iban">IBAN*</label>
                <input id="iban" name="iban" type="text" placeholder="Trinity" class="required">
                <p>(*) Required</p>
            </fieldset>

            <h3>Finish</h3>
            <fieldset>
                <legend>Terms and Conditions</legend>

                <input id="acceptTerms-2" name="acceptTerms" type="checkbox" class="required"> <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
            </fieldset>
        </form>
    </div>

</div>

    </body>

   <!--- Scripts  -->
    <script src="{{ asset('js/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.steps.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
</html>
