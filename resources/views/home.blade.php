<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/apple-icon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('img/favicon.png')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>User Registration</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <!--     Fonts and icons     -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">

    <!-- CSS Files -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/gsdk-bootstrap-wizard.css')}}" rel="stylesheet" />

</head>

<body>
 <div class="image-container set-full-height" style="background-color:black">
    <!--   Big container   -->
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">

                <!--      Wizard container        -->
                <div class="wizard-container">

                    <div class="card wizard-card" data-color="blue" id="wizardProfile">
                        <form id="user-form">

                            {{ csrf_field() }}
                            <div class="wizard-header">
                                <h3>
                                    <b>WUNDER FLEET</b> USER REGISTRATION <br>
                                    <small>This information will let us know more about you.</small>
                                </h3>
                            </div>

                            <div class="wizard-navigation">
                                <ul>
                                    <li><a href="#profile" data-toggle="tab">Personal Profile</a></li>
                                    <li><a href="#address" data-toggle="tab">Address</a></li>
                                    <li><a href="#payment" data-toggle="tab">Payment</a></li>
                                    <li><a href="#confirmation" data-toggle="tab">Confirmation</a></li>
                                </ul>

                            </div>

                            <div class="tab-content">
                                <div class="tab-pane" id="profile">
                                    <div class="row">
                                        <h4 class="info-text"> Let's start with the basic information</h4>
                                        <div class="col-sm-10 col-sm-offset-1">
                                            <div class="form-group">
                                            <label>First Name <small>(required)</small></label>
                                            <input name="first_name"  id="first_name" type="text" class="form-control" placeholder="Neo" required="true">
                                            </div>
                                            <div class="form-group">
                                                <label>Last Name <small>(required)</small></label>
                                                <input name="last_name" id="last_name" type="text" class="form-control" placeholder="Trinity" required="true">
                                            </div>
                                            <div class="form-group">
                                                <label>Telephone <small>(required)</small></label>
                                                <input name="telephone" id="telephone" type="tel" class="form-control" placeholder="+44 xxxxxxxxxxxx" required="true">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="payment">
                                    <h4 class="info-text"> Payment Detail </h4>
                                    <div class="row">

                                        <div class="col-sm-10 col-sm-offset-1">
                                            <div class="form-group">
                                                <label>Account Owner <small>(required)</small></label>
                                                <input name="account_owner"  id="account_owner" type="text" class="form-control" placeholder="Neo" required="true">
                                            </div>
                                            <div class="form-group">
                                                <label>IBAN <small>(required)</small></label>
                                                <input name="IBAN" id="iban" type="text" class="form-control" placeholder="Trinity" required="true">
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane" id="address">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h4 class="info-text"> Where are you located?</h4>
                                        </div>
                                        <div class="col-sm-5 col-sm-offset-1">
                                            <div class="form-group">
                                                <label>Street Name<small>(required)</small></label>
                                                <input name="street_name" id="street_name" type="text" class="form-control" placeholder="10th Street" required="true">
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label>House Number<small>(required)</small></label>
                                                <input name="house_number" id="house_number" type="text" class="form-control" placeholder="242" required="true">
                                            </div>
                                        </div>

                                        <div class="col-sm-3 col-sm-offset-1">
                                            <div class="form-group">
                                                <label>Zip Code</label>
                                                <input id="zip_code" name="zip_code" type="number" class="form-control" placeholder="332" required="true">
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label>City</label><br>
                                                <select name="city" id="city" class="form-control">
                                                    <option value="Amsterdam"> Amsterdam </option>
                                                    <option value="Alicante"> Alicante </option>
                                                    <option value="Barcelona"> Barcelona </option>
                                                    <option value="Berlin"> Berlin </option>
                                                    <option value="Cardiz"> Cardiz </option>
                                                    <option value="Dusseldorf"> Dusseldorf </option>
                                                    <option value="Hamburg"> Hamburg </option>
                                                    <option value="London"> London </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="confirmation">
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <h4 class="info-text"> Payment Response</h4>
                                        </div>

                                        <div class="col-sm-12" id="show_error">
                                            <span class="error-code text-danger text-center">Failed Transaction,Please check transaction details and try again</span>
                                        </div>

                                        <div class="col-sm-12 justify-content-center " id="show_success">
                                            <span class="success-code text-center text-info"><b>Thank You For Joining Us.Enjoy the ride!!</b></span></br>
                                            <span class="success-code text-center text-info"><b>Payment Code Below</b></span></br>
                                            <span class="success-code"><b><label id="payment_code"></label></b></span>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="wizard-footer height-wizard">
                                <div class="pull-right">
                                    <input type='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' name='next' value='Next' />
                                    <input type='button' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Finish' />

                                </div>

                                <div class="pull-left">
                                    <input  type='button' id="previous" class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </form>
                    </div>
                </div> <!-- wizard container -->
            </div>
        </div><!-- end row -->
    </div> <!--  big container -->

    <div class="footer">
        <div class="container">
           Wunder Fleet User Registration Copyright &copy; 2019
        </div>
    </div>

</div>

</body>

<!--   Core JS Files   -->
<script src="{{asset('js/jquery-2.2.4.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.bootstrap.wizard.js')}}" type="text/javascript"></script>

<!--  Plugin for the Wizard -->
<script src="{{asset('js/gsdk-bootstrap-wizard.js')}}" type="text/javascript"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="{{asset('js/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/custom.js')}}" type="text/javascript"></script>
</html>
