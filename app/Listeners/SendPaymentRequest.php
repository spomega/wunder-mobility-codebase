<?php

namespace App\Listeners;

use App\Events\CustomerRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SendPaymentRequest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerRegistered  $event
     * @return void
     */
    public function handle(CustomerRegistered $event)
    {
        $parameters = $event->data;
        //
        $url = config('services.payment.url');

        try {
            //send payment request to server
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "{  \n  \"customerId\": " . $parameters["customerId"] . ",  \n  \"iban\": \"" . $parameters["iban"] . "\",  \n  \"owner\": \"" . $parameters["owner"] . "\"  \n }");
            curl_setopt($ch, CURLOPT_POST, 1);


            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }

            curl_close($ch);

            return $result;

        } catch (\Exception $e) {

            Log::error($e->getMessage());
            return null;

        }
    }

}
