<?php

namespace App\Http\Controllers;

use App\Events\CustomerRegistered;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;

class CustomerController extends Controller
{

     protected $repository;

    /**
     * CustomerController constructor.
     * @param $repository
     */
    public function __construct(CustomerRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCustomer(Request $request)
    {

        //save customer info and make payment request


        $customer = $this->repository->create($request->all());


        // build payment data
        $data = array('customerId' => $customer->id,'iban' => $request->input('IBAN'),'owner' => $request->input('account_owner'));


        $result = event(new CustomerRegistered($data));

        $payment_code = json_decode($result[0]);

         if($payment_code){

             //save payment id and update status

             $customer->payment_id = $payment_code->paymentDataId;
             $customer->payment_status = "DONE";
             $customer->save();

             return json_encode($payment_code->paymentDataId);

         }

    }


}
