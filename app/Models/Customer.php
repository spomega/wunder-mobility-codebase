<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /*
     * Mass assignable attributes
     *
     */

    protected  $fillable = [
        'first_name',
        'last_name',
        'telephone',
        'street_name',
        'house_number',
        'zip_code',
        'city',
        'account_owner',
        'IBAN'];
}
