<?php

namespace App\Repositories;


use App\Models\Customer;
use App\Repositories\Interfaces\RepositoryContract;
use Illuminate\Database\QueryException;

class CustomerRepository implements RepositoryContract
{

   // protected $model;

    /**
     * CustomerRepository constructor.
     */
    public function __construct(Customer $customer)
    {
        $this->model  = $customer;
    }

    public function all(array $columns = ['*'])
    {
        // TODO: Implement all() method.
    }

    public function count()
    {
        // TODO: Implement count() method.
    }

    /**
     * @param array $data
     * return Customer
     */
    public function  create(array $data): Customer
    {

        try{
            return $this->model->create($data);
        }catch(QueryException $e){
            echo $e->getMessage();
            echo  $e->getTraceAsString();
            return null;
        }
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }

    public function deleteById($id)
    {
        // TODO: Implement deleteById() method.
    }
}
