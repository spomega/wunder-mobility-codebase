<?php


namespace App\Repositories\Interfaces;


interface RepositoryContract
{
    public function all(array $columns = ['*']);

    public function count();

    public function create(array $data);

    public function delete();

    public function deleteById($id);


}
