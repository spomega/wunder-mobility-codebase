jQuery(document).ready(function() {


    //set respective cookies to their input fields
    $("#first_name").val(getCookieValue('first_name'));
    $("#last_name").val(getCookieValue('last_name'));
    $("#telephone").val(getCookieValue('telephone'));

    $("#street_name").val(getCookieValue('street_name'));
    $("#house_number").val(getCookieValue('house_number'));
    $("#zip_code").val(getCookieValue('zip_code'));
    $("#city").val(getCookieValue('city'));

    $("#account_owner").val(getCookieValue('account_owner'));
    $("#iban").val(getCookieValue('iban'));


    $("#show_success").hide();

    $("#show_error").hide();

});

function removeCookie(name) {
    document.cookie = name+'=; Max-Age=-99999999;';
}

function setCookie(name,value) {
    var days = 5;
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    console.log("Cookie set " + value)
}

function getCookieValue(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
