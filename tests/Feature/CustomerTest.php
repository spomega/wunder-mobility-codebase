<?php

namespace Tests\Feature;

use App\Models\Customer;
use App\Repositories\CustomerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomerTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;
    /**
     * @test
     * Test for home page access.
     *
     * @return void
     */
    public function user_can_access_home_page()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * @test
     * Test for creating user.
     *
     * @return void
     */

    public function user_can_create_account(){

        $userInfo = [

            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'telephone' => $this->faker->unique()->phoneNumber,
            'street_name' => $this->faker->streetName,
            'house_number' => $this->faker->streetAddress,
            'zip_code' => $this->faker->postcode,
            'city' => $this->faker->city,
            'account_owner' => $this->faker->name,
            'IBAN' => $this->faker->iban('DE')

        ];

        $customerRepository = new CustomerRepository(new Customer());
        $customer = $customerRepository->create($userInfo);


        $this->assertInstanceOf(Customer::class, $customer);
        $this->assertEquals($userInfo['first_name'], $customer->first_name);
        $this->assertEquals($userInfo['last_name'], $customer->last_name);
        $this->assertEquals($userInfo['telephone'], $customer->telephone);

    }


    /**
     * @test
     * Test for payment process .
     *
     * @return void
     */
    public function user_can_send_details_and_get_payment_id(){

        $url = config('services.payment.url');

        $customer = factory(Customer::class)->create();

        //send payment request to server
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{  \n  \"customerId\": " . $customer->id . ",  \n  \"iban\": \"" . $customer->IBAN . "\",  \n  \"owner\": \"" . $customer->account_owner. "\"  \n }");
        curl_setopt($ch, CURLOPT_POST, 1);


        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals(200,$httpcode);

        $this->assertContains('paymentDataId',$result);

    }

    /**
     * @test
     * Test for payment process .
     *
     * @return void
     */
    public function request_can_be_a_bad_request(){

        $url = config('services.payment.url');


        //send payment request to server
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{  \n  \"customerId\": " . 4 . ",  \n  \"ibanE\": \"" . " " . "\",  \n  \"owner\": \"" . "Max Mustermann" . "\"  \n }");
        curl_setopt($ch, CURLOPT_POST, 1);


        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals(400,$httpcode);

    }

}
